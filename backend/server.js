const express = require ('express')
const db = require('./db')

const app = express()
app.use(express.json())


//1. GET  --> Display Movie using name from Containerized MySQL

app.get("/", (request, response) => {
    const connection = db.openConnection()
    const sql = `SELECT * FROM movie`
    connection.query (sql, (error, data) => {
        connection.end()
        response.send(data)
    })
})


//2. POST --> ADD Movie data into Containerized MySQL table

app.post("/", (request, response) => {
    const connection = db.openConnection()
    //movie_id, movie_title, movie_release_date,movie_time,director_name
    const {movie_id, movie_title, movie_release_date,movie_time,director_name} = request.body
    const sql = `INSERT INTO movie VALUES (${movie_id}, ${movie_title}, ${movie_release_date}, ${movie_time}, ${director_name})`
    connection.query (sql, (error, data) => {
        connection.end()
        response.send(data)
    })
})


//3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table

app.put("/:movie_id", (request, response) => {
    const connection = db.openConnection()
    const {movie_id} = request.params
    const {movie_release_date, movie_time} = request.body
    const sql = `UPDATE movie SET movie_release_date = '${movie_release_date}',
    movie_time = '${movie_time}' WHERE movie_id = '${movie_id}' `
    connection.query (sql, (error, data) => {
        connection.end()
        response.send(data)
    })
})

//4. DELETE --> Delete Movie from Containerized MySQL

app.delete("/:movie_id", (request, response) => {
    const connection = db.openConnection()
    const {movie_id} = request.params
    const sql = `DELETE from movie WHERE movie_id = '${movie_id}' `
    connection.query (sql, (error, data) => {
        connection.end()
        response.send(data)
    })
})

app.listen(4000, () => {
    console.log("listening at port 4000 !")
})